<?php

function playBlackjack($inputs) {
    $total = 0;

    foreach ($inputs as $input) {
        $card = getCardFaceValue($input);
        $total = calculateTotal($card, $total);
    }

    return $total;
}

function calculateTotal($card, $total) {

    switch ($card) {
        case 'K':
        case 'Q':
        case 'J':
            $total += 10;
            break;
        case 'A':
            $total += 11;
            break;
        case is_numeric($card):
            $total += (int)$card;
            break;
        default:
            $total += 0;
    }

    return $total;
}

function hitWinOrBust($score) {
    switch ($score) {
        case 21:
            return 'Blackjack!';
        case $score > 21:
            return 'Bust';
        default:
            return 'Hit';
    }
}

function getCardFaceValue($card) {
    return substr($card, 0, strlen($card) - 1);
}

function getCardSuit($card) {
    return substr($card, -1, 1);
}

function isValidCardFaceValue($card) {
    $value = getCardFaceValue($card);
    return preg_match("/^([2-9]|[1][0]|[AQKJ])/", $value);
}

function isValidCardSuit($card) {
    $value = getCardSuit($card);
    return preg_match("/[CDHS]/", $value);
}

function areValidCards($cards) {
    $valid = true;
    foreach ($cards as $card) {
        $valid = $valid && isValidCardFaceValue($card) && isValidCardSuit($card);
    }

    return $valid;
}

function clean($input) {
    $input = strtoupper($input);
    return preg_replace("/[^A-Z0-9]/", '', $input);
}
