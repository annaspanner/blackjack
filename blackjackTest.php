<?php

require_once('blackjack.php');

class DataTest extends PHPUnit_Framework_TestCase {

    protected $validCards = array('2H','5D','8H','10C','KD','JS','QC','AD');
    protected $invalidCards = array('1H','20D','2W','WP','T5','4Q','10A','AP');

    /**
     * @dataProvider additionProvider
     */
    public function testAdd($card1, $card2, $totalExpected) {
        $total = 0;
        $total = calculateTotal($card1, calculateTotal($card2, $total));
        $this->assertEquals($totalExpected, $total);
    }

    public function testHitWinOrBust() {
        // Total less than 21
        for($score = 2; $score < 21; $score++) {
            $this->assertEquals(hitWinOrBust($score), 'Hit');
            $this->assertNotEquals(hitWinOrBust($score), 'Blackjack!');
            $this->assertNotEquals(hitWinOrBust($score), 'Bust');
        }
        // Total equals 21
        $this->assertNotEquals(hitWinOrBust(21), 'Hit');
        $this->assertEquals(hitWinOrBust(21), 'Blackjack!');
        $this->assertNotEquals(hitWinOrBust(21), 'Bust');
        // Total greater than 21
        $this->assertNotEquals(hitWinOrBust(22), 'Hit');
        $this->assertNotEquals(hitWinOrBust(22), 'Blackjack!');
        $this->assertEquals(hitWinOrBust(22), 'Bust');
    }

    /**
      * @dataProvider cardProviderHappy
      */
    public function testFaceValueHappy($card, $faceExpected, $suitExpected) {
        $face = getCardFaceValue($card);
        $this->assertEquals($face, $faceExpected);
    }

    /**
      * @dataProvider cardProviderSad
      */
    public function testFaceValueSad($card, $faceExpected, $suitExpected) {
        $face = getCardFaceValue($card);
        $this->assertNotEquals($face, $faceExpected);
    }

    /**
      * @dataProvider cardProviderHappy
      */
    public function testSuitHappy($card, $faceExpected, $suitExpected) {
        $suit = getCardSuit($card);
        $this->assertEquals($suit, $suitExpected);
    }

    /**
      * @dataProvider cardProviderSad
      */
    public function testSuitSad($card, $faceExpected, $suitExpected) {
        $suit = getCardSuit($card);
        $this->assertNotEquals($suit, $suitExpected);
    }

    public function testValidCards() {
        $this->assertTrue(areValidCards($this->validCards));
        $this->assertFalse(areValidCards($this->invalidCards));
    }

    public function testClean() {
        $this->assertEquals(clean('#A_.%!H*]?'), 'AH');
    }

    public function additionProvider() {
        return array(
            array('A', 10,  21),
            array('A', 'A', 22),
            array(2,   5,   7),
            array('Q', 3,   13),
            array(8,   'K', 18),
            array(10,  10,  20),
            array('J', 6,   16),
            array('K', 'J', 20),
        );
    }

    public function cardProviderHappy() {
        return array(
            array('AS', 'A', 'S'),
            array('KC', 'K', 'C'),
            array('QH', 'Q', 'H'),
            array('JD', 'J', 'D'),
            array('10S', 10, 'S'),
            array('7C',  7,  'C'),
            array('5H',  5,  'H'),
            array('2D',  2,  'D'),
        );
    }

    public function cardProviderSad() {
        return array(
            array('AS',  10,  'D'),
            array('KC',  5,   'H'),
            array('QH',  'K', 'C'),
            array('KD',  'J', 'S'),
            array('10S', 3,   'D'),
            array('7C',  'A', 'H'),
            array('5H',  9,   'C'),
            array('2D',  10,  'S'),
        );
    }

}
