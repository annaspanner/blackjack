<?php

require_once('blackjack.php');

$isValid = true;
$message = 'Enter two cards to play';
$class = '';

if (!empty($_POST)) {
    $inputs = array(clean($_POST['Card1']), clean($_POST['Card2']));
    $isValid = areValidCards($inputs);

    if ($isValid) {
        $score = playBlackjack($inputs);
        $outcome = hitWinOrBust($score);
        $message = "Your score is $score ($outcome)";
        $class = 'success';
    } else {
        $message = 'Invalid cards, try again';
        $class = 'error';
    }
}
?>

<html>
    <head>
        <title>Blackjack</title>
        <link rel="stylesheet" type="text/css" href="blackjack.css">
    </head>
    <body>
        <h1>Blackjack</h1>
        <div class="message <?php print $class; ?>">
            <?php print $message; ?>
        </div>
        <form action="" method="post" name="blackjackform">
            <div class="field">
                <label for="Card1">Card 1:</label>
                <input id="Card1" name="Card1" type="text" />
            </div>
            <div class="field">
                <label for="Card2">Card 2:</label>
                <input id="Card2" name="Card2" type="text" />
            </div>
            <div class="field">
                <input id="submit" name="submit" type="submit" value="Play" />
            </div>
        </form>
    </body>
</html>
